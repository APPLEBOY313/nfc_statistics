/*

	Stellar by HTML5 UP

	html5up.net | @ajlkn

	Free for personal and commercial use under the CCA 3.0 license (html5up.net/license)

*/



/******************** global variable and constant define ************************/





/****************  CSV data load *****************/

	var   rows;
	var   filteredData;
	var   sTable = "";
	var   selected_team = "NE";
	var   oRow = "";		//team option tag array variable
	var   sort_oRow=[];	//team option data array variable.

	const WEEK_TABLE = '1';
	const TEAM_TABLE = '2';
	const STRATEGY_TABLE = '3';

/************************* main code **************************/

(function($) {
	skel.breakpoints({
		xlarge: '(max-width: 1680px)',
		large: '(max-width: 1280px)',
		medium: '(max-width: 980px)',
		small: '(max-width: 736px)',
		xsmall: '(max-width: 480px)',
		xxsmall: '(max-width: 360px)'
	});

	$(function() {
		var	$window = $(window),
			$body = $('body'),
			$main = $('#main');
		// Disable animations/transitions until the page has loaded.
			$body.addClass('is-loading');
			$window.on('load', function() {
				window.setTimeout(function() {
					$body.removeClass('is-loading');
				}, 100);
				load_table();
				$(".mcs-horizontal-example").mCustomScrollbar({
				  axis:"x",
				  theme:"dark-3"
				});
			});
		// Fix: Placeholder polyfill.
			$('form').placeholder();
		// Prioritize "important" elements on medium.
			skel.on('+medium -medium', function() {
				$.prioritize(
					'.important\\28 medium\\29',
					skel.breakpoint('medium').active
				);
			});
		// Nav.
			var $nav = $('#nav');
			if ($nav.length > 0) {
				// Shrink effect.
				$main
					.scrollex({
						mode: 'top',
						enter: function() {
							$nav.addClass('alt');
						},
						leave: function() {
							$nav.removeClass('alt');
						},
					});
				// Links.
				var $nav_a = $nav.find('a');
				$nav_a
					.scrolly({
						speed: 1000,
						offset: function() { return $nav.height()+30; }
					})
					.on('click', function() {
						var $this = $(this);
						// External link? Bail.
							if ($this.attr('href').charAt(0) != '#')
								return;
						// Deactivate all links.
							$nav_a
								.removeClass('active')
								.removeClass('active-locked');
						// Activate link *and* lock it (so Scrollex doesn't try to activate other links as we're scrolling to this one's section).
							$this
								.addClass('active')
								.addClass('active-locked');
					})

					.each(function() {
						var	$this = $(this),
						id = $this.attr('href'),
						$section = $(id);
						// No section for this link? Bail.
						if ($section.length < 1)
							return;
						// Scrollex.
						$section.scrollex({
							mode: 'middle',
							initialize: function() {
								// Deactivate section.
									if (skel.canUse('transition'))
										$section.addClass('inactive');
							},
							enter: function() {
								// Activate section.
									$section.removeClass('inactive');
								// No locked links? Deactivate all links and activate this section's one.
									if ($nav_a.filter('.active-locked').length == 0) {
										$nav_a.removeClass('active');
										$this.addClass('active');
									}
								// Otherwise, if this section's link is the one that's locked, unlock it.
									else if ($this.hasClass('active-locked'))
										$this.removeClass('active-locked');
							}
						});
					});
			}
		// Scrolly.
			$('.scrolly').scrolly({
				speed: 1000
			});
	});
})(jQuery);

function load_table() 
{
	let i=0;
	var today = new Date();
	var start_time = new Date("Sep 06 2017 0:0:0 GMT+3");
	var scale = start_time.getTime();
	var counter = today.getTime();
	var time_differ = counter-scale;
	var one_week = 1000 * 60 * 60 * 24 * 7;
	var mod = time_differ % one_week;
	var week_count = (time_differ - mod)/one_week + 1;


	//------------------ loading strategy table---------------------

	csvLoad(WEEK_TABLE,week_count,"");
	$('#week_sel').val(week_count);
	csvLoad_text("week_text","1","");
	//----------------------------------------------------------

	let timerId = setTimeout(function tick() {
		if(i < 3)
		{
			if (i == 0) 
			{
				//------------------ loading strategy table---------------------
				csvLoad(TEAM_TABLE,"",selected_team)
				csvLoad_text("team_text","",selected_team)
				//----------------------------------------------------------				
	  			timerId = setTimeout(tick, 800); // (*)
			}
			if (i == 1) 
			{
				//------------------ loading strategy table---------------------
				csvLoad(STRATEGY_TABLE,"","");
	  			timerId = setTimeout(tick, 200); // (*)
				//----------------------------------------------------------
			}
			if (i == 2) 
			{
				//------------------ loading strategy table---------------------
				jump_fun(jump);
				//----------------------------------------------------------
			}
			i++;
		}
	}, 800);
}

function jump_fun(jump_id)
{
	if(jump_id !='')
	{
		scrollToID(jump_id, 800);
		jump='';
	}
}

$("#week_tbody").on("click", "td", function() {
	var cs1 = $( this ).attr("class");
	if(cs1 === "selectable"){
	    selected_team=$( this ).text();
		csvLoad(TEAM_TABLE,"",selected_team);
		$('#team_select_id').val(selected_team);
		csvLoad_text("team_text","",selected_team);
        scrollToID("team", 800);
	}
});

$('#week_sel').on('change', function (e) {
    var optionSelected = $('#week_sel').val();
    weeknum=optionSelected;
	csvLoad(WEEK_TABLE,weeknum,"");
	csvLoad_text("week_text",weeknum,"");
});

$('#team_select_id').on('change', function (e) {
    var optionSelected = $('#team_select_id').val();
    selected_team=optionSelected;
	csvLoad(TEAM_TABLE,"",selected_team);
	csvLoad_text("team_text","",selected_team);
});

function csvLoad_text(txt_id,weeknum,team) {
	var csv_url = "";
	var txtRow = "";
	var fltTxt = "";
	if(txt_id === "week_text"){
		csv_url="assets/csv/SpreadSpokeData_week_text.csv";
	}
	else if(txt_id === "team_text")	{
		csv_url="assets/csv/SpreadSpokeData_team_text.csv";
	}
	d3.csv(csv_url, function(loadedRows) {
	  	txtRow = loadedRows;
	  	fltTxt="";
		fltTxt = loadedRows.filter(function(d) { 
			if(txt_id === "week_text"){
		        if( d["week"] == weeknum) { 
		            return d;
		        } 
			}
			else if(txt_id === "team_text")	{
		        if( d["team"] == team) 
		        { 
		            return d;
		        } 			
			}
	    })
		if(txt_id === "week_text"){
			$("#week_text").text(fltTxt[0]["week_text"]);
		}
		if(txt_id === "team_text"){	
			//$("#team_text").text(fltTxt[0]["team_text"]);
		}
	});	
}

function csvLoad(table_num,weeknum,team){
	var csv_url="";
	var table_id="";
	if(table_num === WEEK_TABLE){
		csv_url="assets/csv/games.csv";
		table_id="week_tbody";
		sTable = "";
		var sRow = "";
		sRow += tag("td","Date","true");
		sRow += tag("td","Home Team","true");
		sRow += tag("td","Away Team","true");
		sRow += tag("td","Winner Pick","true");
		sRow += tag("td","Spread Pick","true");
		sRow += tag("td","Over/Under Pick","true");
		sTable = tag("tr",sRow,"true");
	}

	else if(table_num === TEAM_TABLE){
		csv_url="assets/csv/teams.csv";
		table_id="team_tbody";
		sTable = "";
		var sRow = "";
		sRow += tag("td","Season","true");
		sRow += tag("td","W","true");
		sRow += tag("td","L","true");		
		sRow += tag("td","T","true");
		sRow += tag("td","Win %","true");
		sRow += tag("td","Cover %","true");
		sRow += tag("td","Over %","true");
		sRow += tag("td","Off Pts/G","true");
		sRow += tag("td","Def Pts/G","true");
		sTable = tag("tr",sRow,"true");
	}

	else if(table_num === STRATEGY_TABLE){
		csv_url="assets/csv/strategies.csv";
		table_id="strategy_tbody";
		sTable = "";
		var sRow = "";
		sRow += tag("td","Rank","true");
		sRow += tag("td","Strategy","true");
		sRow += tag("td","Bet","true");
		sRow += tag("td","Bet Win %","true");
		sTable = tag("tr",sRow,"true");
	}

	//-------------- reading and filtering csv file -------------//
	d3.csv(csv_url, function(loadedRows) {
	  	rows = loadedRows;
	  	filteredData="";
	  	// doSomethingWithRows();
		filteredData = loadedRows.filter(function(d) { 
			if(table_num === WEEK_TABLE){
		        if( d["schedule_week"] == weeknum) {
		            return d;
		        } 
			}
			else if(table_num === TEAM_TABLE){
		        if( d["Team"] == team) { 
		            return d;
		        } 			
			}
	    })
	    if(selected_team === ""){
	    	selected_team = filteredData[0]["Home Team"];
	    }
	    if(table_num === STRATEGY_TABLE){
			//-------- insert strategy table ---------//
			csvToHtml(rows,table_id,table_num);
	    }
	    else{
	    	if(table_num == 2){
	    		if(filteredData == ""){
	    			alert("There is no data for selected team.");
	    		}
	    		else{
					csvToHtml(filteredData,table_id,table_num);	
	    		}
	    	}
	    	else{
				//-------- insert week and team table ---------//
				csvToHtml(filteredData,table_id,table_num);		    		
	    	}
	    }
	});
}	

function csvToHtml(sCSV, table_id,table_num) {
	$("#"+table_id).html(toHTML(sCSV,table_num));
	if(table_num === WEEK_TABLE){
		$("#team_select_id").html(oRow);
	}		
}

function toHTML(arr,table_num) {
	var sRow = "";
	oRow = "";
	if(table_num == WEEK_TABLE){
		for (var i = 0; i < arr.length; i++) {
			sRow += tag("td",arr[i]["Date"],"false");
			sRow += tag("td",arr[i]["Home Team"],"false","selectable");
			sRow += tag("td",arr[i]["Away Team"],"false","selectable");
			sRow += tag("td",arr[i]["Winner Pick"],"false");
			sRow += tag("td",arr[i]["Spread Pick"],"false");
			sRow += tag("td",arr[i]["Over/Under Pick"],"false");
			// Team selet option tag data array variable
			sort_oRow.push(arr[i]["Home Team"]);
			sort_oRow.push(arr[i]["Away Team"]);
			sTable += tag("tr",sRow,"false");
			sRow = "";
		}
		// sorting team select option tag data and store in the oRow
		oRow="";
		sort_oRow.sort();
		for (var i = 0; i < sort_oRow.length; i++) {
			oRow += tag_option(sort_oRow[i]);
		}
	}

	else if(table_num == TEAM_TABLE){
		for (var i = 0; i < arr.length; i++) {
			sRow += tag("td",arr[i]["Season"],"false");
			sRow += tag("td",arr[i]["W"],"false");
			sRow += tag("td",arr[i]["L"],"false");
			sRow += tag("td",arr[i]["T"],"false");
			sRow += tag("td",arr[i]["Win %"],"false");
			sRow += tag("td",arr[i]["Cover %"],"false");
			sRow += tag("td",arr[i]["Over %"],"false");
			sRow += tag("td",arr[i]["Off Pts/G"],"false");
			sRow += tag("td",arr[i]["Def Pts/G"],"false");
			sTable += tag("tr",sRow,"false");
			sRow = "";
		}
	}
	else
	{
		for (var i = 0; i < arr.length; i++) {
			sRow += tag("td",arr[i]["Rank"],"false");
			sRow += tag("td",arr[i]["Strategy"],"false");
			sRow += tag("td",arr[i]["Bet"],"false");
			sRow += tag("td",arr[i]["Bet Win %"],"false");
			sTable += tag("tr",sRow,"false");
			sRow = "";
		}
	}
	return sTable;
}

function tag(element, value, thead,selectable="undefine") {
	var ret_val="";
	if(thead == 'true')	{
		ret_val = "<" + element + " style='background-color:#FD913A;'>" + "<strong style='color: #fff; font-weight:bold;'>" + value + "</strong>" + "</"+ element + ">";		
	}
	else if(selectable == "undefine"){
		ret_val = "<" + element + ">" + value + "</" + element + ">";
	}
	else{
		ret_val = "<" + element + " class='selectable'><a href=\"#team\">" + value + "</a></" + element + ">";
	}
	return ret_val;
}

function tag_option( value ) {
	return "<option value=\""+ value +"\">" + value  + "</option>";
}

// scrollToID function
function scrollToID(id, speed) {
    var offSet = 100;
    var obj = $('#' + id);
    if(obj.length){
      var offs = obj.offset();
      var targetOffset = offs.top - offSet;
      $('html,body').animate({ scrollTop: targetOffset }, speed);
    }
}

$('#signup').click(function(){
	var email = $('#mail').val();
	$.get( "email.php", { email: email } )
	.done(function( data ) {
		window.location="https://spreadspoke.com/upwork/Game/index.html";
	});
});



