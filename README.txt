
Vibrant color palette which can always tone down if it's a little too vibrant
"sticky" in-page nav bar (powered by my Scrollex plugin)
a separate generic page template (just in case you need one),
and an assortment of pre-styled elements.

	Icons:
		Font Awesome (fortawesome.github.com/Font-Awesome)

	Other:
		jQuery (jquery.com)
		html5shiv.js (@afarkas @jdalton @jon_neal @rem)
		CSS3 Pie (css3pie.com)
		Respond.js (j.mp/respondjs)
		Scrollex (@ajlkn)
		Skel (skel.io)